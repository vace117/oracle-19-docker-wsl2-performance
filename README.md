This project is used as a simple demonstration of a performance problem that occurs when running Oracle 19 in Docker on WSL2, vs running Oracle 12 natively on Windows.

The entire project consists of a single Java JUnit test, which clearly illustrates the problem by timing 3 simple test scenarios. The test is first executed for Oracle 12 running natively on Windows, and then again for Oracle 19 deployed in a Docker container running in WSL2. The results can then be compared to each other and the performance problem can be clearly seen. 

# Test Scenarios
## 1) JDBC Connection performance test
This test repeatedly establishes a new JDBC connection and performs a small query that should not use any Disk I/O. This is done in a loop of 100 iterations and the total time is measured. This test attempts to measure the overhead of establishing a JDBC connection.

The test query:
```sql
select 1 from dual
```

## 2) Big Data Generate and Fetch test
This test executes a query which generates a lot of data in memory, and then returns that data. This test measures query performance and JDBC transport time, hopefully without any disk I/O.

The test query:
```sql
select dbms_random.value(0, 10000000000) from dual connect by level <= 100000
```

## 3) Big Data Fetch test
This is a lot like `Test #2`, but the data is pre-generated and stored in `BIG_DATA` table, so this test measures just the query performance reading from a table, and JDBC transport time. 

`BIG_DATA` table was pre-created as follows:
```sql
CREATE TABLE BIG_TABLE (
  RN_1 INTEGER NOT NULL,
  RN_2 INTEGER NOT NULL
)

INSERT INTO BIG_TABLE (RN_1, RN_2)
select 
  trunc(dbms_random.value(1,10000000000)) RN_1,
  trunc(dbms_random.value(1,10000000000)) RN_2 
from dual
connect by level <= 2000000;
```

And the test query:
```sql
SELECT * FROM BIG_TABLE fetch first 100000 rows only
```

## 2) and 3) with "Fetch Size" set to 100,000
I also ran Tests #2 and #3 with `Fetch Size` set to 100,000, instead of the default 10. 

The queries ran about 16 times faster, with dockerized Oracle being noticably faster than native Oracle!

Details of `Fetch Size` connection setting: https://docs.oracle.com/cd/A97335_02/apps.102/a83724/oraperf2.htm#1002425

# Test Results
The results of these tests are as follows:
https://docs.google.com/spreadsheets/d/1qgHw23UQG_NbKKF7XuPm9JIFJQQckTGtEJY_DPDkNwY

If you look at the spreadsheet you'll see that the **tests running against Oracle 19 in Docker are 80% slower!**. 

I have verified that the Oracle's docker container has access to all CPUs and 26G of RAM.

## Hypothesis
I suspect the problem might be with the network stack. The reasons I think that might be the case:
* Heavy operations that run inside the db (such as generation of data for `BIG_TABLE`) actually run slighty faster on dockerized Oracle 19 than they do on native Oracle 12. So it's not at all slow, once the operation is running inside the database.
* `Test #1` that simply opens and closes JDBC connections is 42% slower for Docker
* `Test #2` and `Test #3` are equally slower on docker (~ 83%), even though one test has disk I/O and the other does not. What they have in common is sending the data back over the JDBC connection.
* The fact that increasing the `Fetch Size` on the connection makes dockerized Oracle work faster than native suggests that it is not the network transfer speed, but rather connection establishment that slows everything down.


# Test System Setup
* Windows: `Version 1909 (OS Build 18363.1082)`
* Docker: `20.10.0, build 7287ab3`
* Native Oracle: `Oracle Database 12c Enterprise Edition Release 12.1.0.2.0 - 64bit Production`
* Docker Oracle: `Oracle Database 19c Enterprise Edition Release 19.0.0.0.0 - Production`

## Oracle 19 image:
The Oracle 19 image was built using the official Dockerfile provided here: 
https://github.com/oracle/docker-images/tree/master/OracleDatabase/SingleInstance/dockerfiles

The following settings were modified in `'docker-images/OracleDatabase/SingleInstance/dockerfiles/19.3.0/dbca.rsp.tmpl'`:
```text
createAsContainerDatabase=false
totalMemory=16384
```

The total memory allocation was cranked up to unreasonable levels, just to eliminate any potential SGA and PGA memory issues. `V$PGASTAT` and `V$SGASTAT` views were used to verify that PGA and SGA limits are indeed set very high (much higher than my Native Oracle 12 instance).
