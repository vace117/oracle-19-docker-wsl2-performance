package vace117.jdbc;

import static vace117.jdbc.JdbcConnectionTester.*;

import org.junit.Test;

/**
 * These tests use a pre-generated table. BIG_TABLE must be pre-created like this:
 * 
 * CREATE TABLE BIG_TABLE (
 *   RN_1 INTEGER NOT NULL,
 *   RN_2 INTEGER NOT NULL
 * )
 * 
 * INSERT INTO BIG_TABLE (RN_1, RN_2)
 * select 
 *   trunc(dbms_random.value(1,10000000000)) RN_1,
 *   trunc(dbms_random.value(1,10000000000)) RN_2 
 * from dual
 * connect by level <= 2000000;
 */
public class JdbcConnectionTests {
	
	JdbcConnectionTester tester = new JdbcConnectionTester(DOCKER_DB_HOST_PORT);

	
	@Test
	public void testConnectionSpeed() {
		
		tester.testConnectionSpeed();
		
	}

	
	@Test
	public void testNetworkSpeed_GenerateDataAndFetch() {
		
		tester.testNetworkSpeed_GenerateDataAndFetch();
		
	}
	
	@Test
	public void testNetworkSpeed_FetchPreGeneratedData() {
		
		tester.testNetworkSpeed_FetchPreGeneratedData();
		
	}
	
}
