package vace117.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Duration;
import java.time.Instant;
import java.util.function.Consumer;

import oracle.jdbc.OracleStatement;

/**
 * These tests use a pre-generated table. BIG_TABLE must be pre-created like this:
 * 
 * CREATE TABLE BIG_TABLE (
 *   RN_1 INTEGER NOT NULL,
 *   RN_2 INTEGER NOT NULL
 * )
 * 
 * INSERT INTO BIG_TABLE (RN_1, RN_2)
 * select 
 *   trunc(dbms_random.value(1,10000000000)) RN_1,
 *   trunc(dbms_random.value(1,10000000000)) RN_2 
 * from dual
 * connect by level <= 2000000;
 */
public class JdbcConnectionTester {

	final int ROWS = 100000;
	
	public static final String NATIVE_DB_HOST_PORT = "localhost:1539";  // Native Oracle 12
	public static final String DOCKER_DB_HOST_PORT = "localhost:51521"; // Docker Oracle 19
	
	private final String dbhostPort;
	
	
	
	public JdbcConnectionTester(String dbhostPort) {
		this.dbhostPort = dbhostPort;
	}


	public static void main(String[] args) {
		System.out.println( "=================== DOCKER TEST ======================" );
		new JdbcConnectionTester(DOCKER_DB_HOST_PORT).testNetworkSpeed_FetchPreGeneratedData();

		System.out.println( "=================== NATIVE TEST ======================" );
		new JdbcConnectionTester(NATIVE_DB_HOST_PORT).testNetworkSpeed_FetchPreGeneratedData();
	}

	
	public void testConnectionSpeed() {
		
		_timeOperationMillis("Total millis: ", () -> {
			for ( int i = 0; i < 100; i++ ) {
				_connectQueryDisconnect(i, "select 1 from dual");
			}
		});
		
	}

	
	public void testNetworkSpeed_GenerateDataAndFetch() {
		
		_timeOperationMillis("Total millis: ", () ->
			 _connectQueryDisconnect(1, 
				"select dbms_random.value(0, 10000000000) from dual connect by level <= " + ROWS
			)			
		);
		
	}
	
	public void testNetworkSpeed_FetchPreGeneratedData() {
		_timeOperationMillis("Total millis: ", 
			() -> _connectQueryDisconnect(1, "SELECT * FROM BIG_TABLE fetch first " + ROWS + "rows only")		
		);		
	}
	

	
	
	private void _connectQueryDisconnect(int round, String query) {
		_connectQueryDisconnect(round, query, rs -> {});
	}
	
	private void _connectQueryDisconnect(int round, String query, Consumer<ResultSet> dataGetter) {
		System.out.println( "========= CONNECTION ROUND " + round + " ==========" );
		
//		_timeOperationMillis("Round millis: ", () -> {
			try ( Connection connection = _establishConnection() ) {
				
				OracleStatement stmt = (OracleStatement) connection.createStatement();
				//stmt.setRowPrefetch(ROWS);
				ResultSet rs = stmt.executeQuery(query);
				
				while(rs.next()) {
					dataGetter.accept(rs);
				}
				
				rs.close();
				
			} catch (Throwable e) {
				throw new RuntimeException(e);
			}
//		});
	}
	
	private void _timeOperationMillis(String text, Runnable operation) {
		Instant start = Instant.now();

		operation.run();
		
		Instant finish = Instant.now();
		final long timeElapsed = Duration.between(start, finish).toMillis();
		System.out.println(text + timeElapsed + "\n\n");
	}
	
	private Connection _establishConnection() throws SQLException {
		return DriverManager.getConnection("jdbc:oracle:thin:@" + dbhostPort + ":xe", "devdba", "password1");
	}

	
}
